#!/bin/bash
###
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained
# with the works, so that any entity that uses the works is notified of this
# instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# this script installs a vulnerability assessment server
# based on kali linux in a virtual machine
# The following tools are installed:
# - etckeeper for /etc/ version management
# - ssmtp as mail agent
# - openvas with all dependencies
# - droopescan
# - wpscan
###

# error handling
set -eu -o pipefail
shopt -s failglob

# check for root
((EUID)) && echo "Must be run as root" && exit 1;

# set directories
my_dir="${0%/*}" # = "$(dirname "$0")"
inst_dir="${my_dir}/install"

# role: purge all gui stuff
apt purge xfce4* lightdm* xserver* x11-xserver-utils *x11*
apt autoremove -y --purge
apt autoclean -y
rm -rf ~/Documents/ ~/Downloads/ ~/Music/ ~/Pictures/ ~/Public/ ~/Templates/ ~/Videos/
rm ~/.xfce4-session.verbose-log

# role: apt upgrade
apt update -y
apt full-upgrade -y

# role: etckeeper, install and configure etckeeper
"${inst_dir}"/etckeeper.sh

# role: ufw, install and setup firewall
"${inst_dir}"/ufw.sh

# install prereqs
apt install -y open-vm-tools pwgen

# role: install and configure ssmtp
"${inst_dir}"/ssmtp.sh

# role: install and configure openvas
"${inst_dir}"/openvas.sh

# role: install optional packages
"${inst_dir}"/packages.sh
