# Vulnerability Assessment System (VAS)

Scripts to setup a Vulnerability Assessment System based on
[Kali](https://www.kali.org), [OpenVAS](http://www.openvas.org/), [droopescan](https://github.com/droope/droopescan),
[WPScan](https://wpscan.org/) and others.

## Installation

1. Install [Kali Linux Xfce](https://www.kali.org/downloads/).

    _Note: Using anoter distribution is not supported, since it makes installation of some components way more complicated._

2. Install `git`:

        sudo apt install git

3. Checkout this repo:

        cd ~
        git clone https://git.uni-konstanz.de/kn/infosec/openvas

4. Change into the repo directory and run the setup script:

        cd ~/openvas
        ./setup_system.sh

    _Note: The setup script must be run as root, which is the standard user in Kali._

## Content and Notes

- `setup_system.sh`

    Purges all GUI stuff, installs essential packages and runs all install scripts in `install/`

- `install/etckeeper.sh`

    Installs etckeeper. Keeps etc in a local git repo. You can and should add a remote repo and [configure etckeeper to autopush to the remote location](https://coderwall.com/p/v1agsg/installing-etckeeper-to-store-config-with-autopush-to-git-in-ubuntu-14-04-lts).

- `install/ufw.sh`

    Installs, configures, and enables the Uncomplicated FireWall.
    Ssh is allowed from all locations, so be sure to properly configure the firewall after setup according to your needs.

- `install/redis.sh`

    Installs, configures, and enables the memcache server redis. Is required by openVAS.

    _Note: The package `openvas` also installs redis-server as dependency if not yet present.
    However, configuration is flawed at the time of this writing. So explicitly installing and configuring
    redis is prefered._

- `install/ssmtp.sh`

    Installs ssmtp, removes sendmail and postfix. Needed by openVAS to send email alerts.

- `install/openvas.sh`

    Installs, configures, updates, and enables openvas.
	The script autogenerates the admin user with a pseudorandom secure password.
	Username and password are printed to stdout.
	You need to take note of that in order to login to the `greenbone security assistant`.

- `install/scanners.sh`

    Installs the CMS scanners `droopescan`, `wpscan`, as well as the wrapper script `cmsscan`.
