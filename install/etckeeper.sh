#!/bin/sh
apt install -y etckeeper

# make .ssh if not yet present
if [ ! -x "${HOME}"/.ssh ]; then
	mkdir -p "${HOME}"/.ssh
	chmod 700 "${HOME}"/.ssh
fi

# generating keys for git
ssh-keygen -t rsa -b 4096 -C "root@openvas" -N '' -f "${HOME}"/.ssh/id_git

echo "Copy the following key as deploy key to your git repo:"
cat "${HOME}"/.ssh/id_git.pub

if [ -x ./etckeeper.conf ]; then
	cp -var ./etckeeper.conf /etc/etckeeper/
fi

# TODO: add git remote
# TODO: add git ssh config
