#!/bin/bash

apt install -y openvas

# enable optional tools and nvts
apt install rpm nsis alien

# write config
openvassd -s >> /etc/openvas/openvassd.conf

# point to correct redis socket
#sed -i 's/kb_location = .*/kb_location = \/var\/run\/redis\/redis-server.sock/' /etc/openvas/openvassd.conf
sed -i 's/kb_location = .*/kb_location = \/var\/run\/redis-openvas\/redis-server.sock/' /etc/openvas/openvassd.conf

# enable nvt signature verification
sed -i 's/nasl_no_signature_check .*/nasl_no_signature_check = no/' /etc/openvas/openvassd.conf

# feed update
openvas-feed-update

# rebuild database
openvasmd --rebuild --progress

# start scanner service
systemctl start openvas-scanner.service

# create admin user
user=admin
role=Admin
pass=$(pwgen -s 30 1)
openvasmd --create-user="$user" --role="$role"
openvasmd --user="$user" --new-password="$pass"

echo "\033[0;31mNew user created: user=$user role=$role pass=$pass"

# generate certificates
openvas-manage-certs -a

# start manager and security assistant
systemctl start openvas-manager.service
systemctl start greenbone-security-assistant.service

# enable all services
systemctl enable openvas-scanner.service
systemctl enable openvas-manager.service
systemctl enable greenbone-security-assistant.service

# check installation
openvas-check-setup
