#!/bin/bash
my_dir="${0%/*}" # = "$(dirname "$0")"
pkg_dir="${my_dir}/pkg"

# install droopescan
apt install -y python-pip
pip install droopescan

# install wpscan
apt install -y wpscan

# install cmsscan
cp -var "${pkg_dir}"/cmsscan.sh /usr/local/bin/cmsscan
chmod +x /usr/local/bin/cmsscan
