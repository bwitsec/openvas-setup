#!/bin/bash
my_dir="${0%/*}" # = "$(dirname "$0")"
conf_dir="${my_dir}/conf"

apt purge -y sendmail postfix
apt install -y ssmtp

# copy over config
if [ -x "${conf_dir}"/ssmtp.conf ]; then
	cp -var "${conf_dir}"/ssmtp.conf /etc/ssmtp/
else
	echo "WARN: no ssmtp config found. Need to manually configure!"
fi
